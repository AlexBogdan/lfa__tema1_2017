#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include <set>
#include <vector>
#include <unordered_map>
#include <stack>
#include <typeinfo>

// Header
std::string banda(10, '#');
auto cap_banda = banda.begin();

class State;
class Label;
class Rule;
class Machine;
class R;
class L;
class W;
class LCond;
class RCond;
void set_transition(std::vector<State*> &v);

void create_machine(std::string name);
void init_1move_l_machine();
void init_1move_r_machine();
void init_1char_w_machine(char c);
void init_move_cond_l_machine(char c);
void init_move_cond_r_machine(char c);
void init_basic_machines();

void print_banda();

State* current_state;	
Machine* current_machine;
Rule* current_rule;
std::string current_set;

std::set<char> alphabet;
std::stack<State*> decisions;
std::list<State*> labels;

std::unordered_map<std::string, std::set<char>* > sets;
std::unordered_map<std::string, State*> machines;
std::stack<State*> process_stack;

// Body

class Machine { 
public:
    std::string name;

   	Machine() {

   	}

    Machine(std::string name) {
    	this->name = name;
    }
};

// Define BasicMachines

// Masina care muta cursorul cu o unitate la dreapta
class R : public Machine {
public:
	std::string name = "R";

	void execute() {
		char c = *cap_banda;
		*cap_banda = *(cap_banda-1);
		*(cap_banda-1) = c;

		cap_banda++;
	}
};

// Masina care muta cursorul cu o unitate la stanga
class L : public Machine {
public:
	std::string name = "L";

	void execute() {
		cap_banda--;

		char c = *cap_banda;
		*cap_banda = *(cap_banda-1);
		*(cap_banda-1) = c;
	}
};

// Masina care muta scrie un caracter pe banda
class W : public Machine {
public:
	std::string name;
	char c;

	W(char c) {
		this->c = c;
		name = c;
	}

	void execute() {
		*cap_banda = c;
	}
};

//   Masina care muta cursorul la dreapta pana cand
// intalneste un anumit caracter
class RCond : public Machine {
public:
	std::string name;
	char c;

	RCond(char c) {
		this->c = c;
		this->name = "R(";
		this->name += c;
		this->name += ")";
	}

	void execute() {
		do {
			char c = *cap_banda;
			*cap_banda = *(cap_banda-1);
			*(cap_banda-1) = c;

			cap_banda++;
		} while(*cap_banda != c);
	}
};

//   Masina care muta cursorul la stanga pana cand
// intalneste un anumit caracter
class LCond : public Machine {
public:
	std::string name;
	char c;

	LCond(char c) {
		this->c = c;
		this->name = "L(";
		this->name += c;
		this->name += ")";
	}

	void execute() {
		do {
			cap_banda--;

			char c = *cap_banda;
			*cap_banda = *(cap_banda-1);
			*(cap_banda-1) = c;
		} while(*cap_banda != c);
	}
};

class RNonCond : public Machine {
public:
	std::string name;
	char c;

	RNonCond(char c) {
		this->c = c;
		this->name = "R(!";
		this->name += c;
		this->name += ")";
	}

	void execute() {
		do {
			char c = *cap_banda;
			*cap_banda = *(cap_banda-1);
			*(cap_banda-1) = c;

			cap_banda++;
		} while(! (*cap_banda != c));
	}
};

class LNonCond : public Machine {
public:
	std::string name;
	char c;

	LNonCond(char c) {
		this->c = c;
		this->name = "L(!";
		this->name += c;
		this->name += ")";
	}

	void execute() {
		do {
			cap_banda--;

			char c = *cap_banda;
			*cap_banda = *(cap_banda-1);
			*(cap_banda-1) = c;
		} while(! (*cap_banda != c));
	}
};

class State {
public:
	std::string type;
	std::string name;
	Machine* machine = NULL;
	State* next_state = NULL;

	std::unordered_map<char, State*> rules;
	std::vector< std::pair<char, char> > variables;
	char current_local_variable = '~';

	State(std::string type) {
		this->type = type;
	}

	void set_name(std::string name) {
		this->name = name;
	}

	void set_machine(Machine* m) {
		this->machine = m;
	}

	void set_next_state(State* s) {
		this->next_state = s;
		s->variables = this->variables;

		//std::cout << "[TRANSFER] " << s->name << " a primit variabile de la " << name << std::endl;
		s->print_variables();
	}

	void add_rule(char c, State* s) {
		rules.insert({c, s});
	}

	void execute_instruction() {
		//std::cout<<"[CAP_BANDA] " << *cap_banda << std::endl;

		if (machine == NULL) {
			for (auto m : machines) {
				if (m.first == name) {
					process_stack.push(current_state->next_state);
					current_state = m.second->next_state;
					//std::cout << "[EXECUTE] Executam o masina separata | "<< current_state->name << " ~ " << name << std::endl;
					return ;
				}
			}
		}


		
		if (((R*) machine)->name == "R") {
			//std::cout << "[EXECUTE] Executam un R |"<<" " << ((R*) machine)->name << std::endl;
			((R*) machine)->execute();
			current_state = next_state;
			return ;
		}

		
		if (((L*) machine)->name == "L") {
			//std::cout << "[EXECUTE] Executam un L |"<<" " << ((L*) machine)->name << std::endl;
			((L*) machine)->execute();
			current_state = next_state;
			return ;
		}

		for (auto c : alphabet) {
			std::string temp_name = "";
			temp_name += c;
			//std::cout << "[TEMP_NAME] " << temp_name << " " << ((W*) machine)->name << std::endl;
			if (((W*) machine)->name == temp_name) {
				//std::cout << "[EXECUTE] Executam un W |"<<" " << ((W*) machine)->name << std::endl;
				((W*) machine)->execute();
				current_state = next_state;
				return ;
			}
			temp_name = "R(";
			temp_name += c;
			temp_name += ")";
			// //std::cout << "[TEMP_NAME] " << temp_name << std::endl;
			if (((RCond*) machine)->name == temp_name) {
				//std::cout << "[EXECUTE] Executam un RCond |"<<" " << ((RCond*) machine)->name << std::endl;
				((RCond*) machine)->execute();
				current_state = next_state;
				return ;
			}
			temp_name = "L(";
			temp_name += c;
			temp_name += ")";
			// //std::cout << "[TEMP_NAME] " << temp_name << std::endl;
			if (((LCond*) machine)->name == temp_name) {
				//std::cout << "[EXECUTE] Executam un LCond |"<<" " << ((LCond*) machine)->name << std::endl;
				((LCond*) machine)->execute();
				current_state = next_state;
				return ;
			}
			temp_name = "R(!";
			temp_name += c;
			temp_name += ")";
			// //std::cout << "[TEMP_NAME] " << temp_name << std::endl;
			if (((RNonCond*) machine)->name == temp_name) {
				//std::cout << "[EXECUTE] Executam un RNonCond |"<<" " << ((RNonCond*) machine)->name << std::endl;
				((RNonCond*) machine)->execute();
				current_state = next_state;
				return ;
			}
			temp_name = "L(!";
			temp_name += c;
			temp_name += ")";
			// //std::cout << "[TEMP_NAME] " << temp_name << std::endl;
			if (((LNonCond*) machine)->name == temp_name) {
				//std::cout << "[EXECUTE] Executam un LNonCond |"<<" " << ((LNonCond*) machine)->name << std::endl;
				((LNonCond*) machine)->execute();
				current_state = next_state;
				return ;
			}
		}

		//std::cout << "[TEMP_NAME] NU AM GASIT NIMIC" << std::endl;

		// Masina nu foloseste un caracter obisnuit, ci o variabila
		for (auto p : current_state->variables) {
			std::string temp_name = "";
			temp_name += p.first;

			//std::cout << "[TEMP_NAME] (" << temp_name << ", " << p.second << ") " << ((W*) machine)->name << std::endl;
			if (((W*) machine)->name == temp_name) {
				//std::cout << " Am gasit !!!" << p.second << std::endl;
				Machine* m = new W(p.second);
				//std::cout << " Am gasit !!!" << p.second << std::endl;
				//std::cout << "[EXECUTE] Executam un W |"<<" " << ((W*) machine)->name << std::endl;
				((W*) m)->execute();
				delete m;

				current_state = next_state;
				return ;
			}
			
		}
		
	}

	void execute_decision(){
		//std::cout << "Variantele posibile pe care putem merge sunt : ";
		for (auto p : rules) {
			//std::cout << p.first << " ";
		}
		//std::cout << std::endl;

		for (auto p : rules){
			for (int i = 0; i < current_state->variables.size(); i++) {
				for (int j = 0; j < p.second->variables.size(); j++) {
					// Daca avem acelasi nume de variabila, copiem valorea in next_state
					if (current_state->variables[i].first ==
						p.second->variables[j].first) {

						p.second->variables[j].second =
							current_state->variables[i].second;
					}
				}
			}
		}

		for (auto p : rules) {
			if (*cap_banda == p.first) {
				//std::cout << "[RULE] Mergem pe regula |"<<" " << p.first << std::endl;
				current_state = p.second;
				//std::cout << "[VARIABILE] Avem variabilele : ";
				for (auto p : current_state->variables) {
					//std::cout << "(" << p.first << " , " << p.second << ") ";
				}
				//std::cout << std::endl;

				// int pos = current_state->variables.size()-1;
				// if (pos >= 0) {
				// 	char last_var_name = current_state->variables[pos].first;
				// 	current_state->variables[pos] = {last_var_name, p.first};
				// 	//std::cout << "[VAR] Am updatat variabila ("<<last_var_name<<" , " << p.first << ")" << std::endl;
				// }

				for (int i = 0; i < current_state->variables.size(); i++) {
					if (current_state->variables[i].first == current_state->current_local_variable) {
						current_state->variables[i].second = p.first;
					//	out << "[VAR] Am updatat variabila ("<<current_state->current_local_variable<<" , " << p.first << ")" << std::endl;
					}
				}
					
				return ;
				
			}
		}
		current_state = NULL;
	}

	void copy_variable_values() {
		
		for (int i = 0; i < current_state->variables.size(); i++) {
			for (int j = 0; j < next_state->variables.size(); j++) {
				// Daca avem acelasi nume de variabila, copiem valorea in next_state
				if (current_state->variables[i].first ==
					next_state->variables[j].first) {

					next_state->variables[j].second =
						current_state->variables[i].second;
				}
			}
		}

		
	}

	void print_variables() {
		//std::cout << "[VARIABILE] Avem variabilele : ";
		for (auto p : current_state->variables) {
			//std::cout << "(" << p.first << " , " << p.second << ") ";
		}
		//std::cout << std::endl;
	}

	void execute() {
	//	out << "[MASINA = " << name << "] [VARIABILE : ";
		for (auto p : current_state->variables) {
		//	out << "(" << p.first << " , " << p.second << ")";
		}
	//	out << "] ";

		//std::cout << "[STATE_CKECK] Executam un (" << type << ") cu numele [" << name << "]";
		if (machine != NULL) {
			//std::cout << " " << machine->name;
		}
		//std::cout << std::endl;
		
		print_variables();
		if (next_state != NULL) {
			copy_variable_values();
		}
		print_variables();

		if (type == "Instruction") {
			execute_instruction();
		}
		else if (type == "Decision") {
			execute_decision();
		}
		else if (type == "Rule") {
			current_state = next_state;
		}
		else if (type == "Label") {
			//std::cout << banda << std::endl;
			current_state = next_state;
		}
		else if (type == "Machine") {
			current_state = next_state;
		}

		
	}
};

// Construim alfabetul

void add_to_alphabet(char c) {
	alphabet.insert(c);
}

// Construim set-urile de caractere\

void create_set(std::string name) {
	std::set<char>* s = new std::set<char>;
	sets.insert({name, s});
	current_set = name;
}

void add_to_set(char c) {
	sets[current_set]->insert(c);
}

// Construim starile (Masini, Etichete, Masini elementare, etc.)

void add_new_state(State* s) {
	current_state->set_next_state(s);
	current_state = s;
}

void create_machine(std::string name) {
	//std::cout << std::endl << "Cream masina " << name << std::endl;
	Machine* m = new Machine(name);
	State* s = new State("Machine");
	s->set_name(name);
	s->set_machine(m);

	machines.insert({name, s});
	current_state = s;

	// add_new_state(s);
}

void create_instruction_elementary(std::string name, Machine* m) {
	State* s = new State("Instruction");
	s->set_name(name);
	s->set_machine(m);

	add_new_state(s);
}

void create_instruction_R() {
	create_instruction_elementary("R", new R());
	//std::cout << "[INSTR]Cream un R | R" << std::endl;
}

void create_instruction_L() {
	create_instruction_elementary("L", new L());
	//std::cout << "[INSTR]Cream un L | L" << std::endl;
}

void create_instruction_W(char c) {
	create_instruction_elementary("W", new W(c));
	//std::cout << "[INSTR]Cream un W | " << c << std::endl;
}

char find_char(std::string text) {
	//std::cout << text << std::endl;
	char caracter;
	if (text.length() == 1) {
		for (char c : alphabet) {
			std::string temp = "";
			temp += c;
			if (temp == text) {
				caracter = c;
				break;
			}
		}
	}
	else {
		for (auto s : sets) {
			if (text == s.first) {
				caracter = *(s.second->begin());
				break;
			}
		}
	}

	return caracter;
}

void create_instruction_RCond(std::string text) {
	char c = find_char(text);
	std::string name = "R(";
	name += c;
	name += ")";
	create_instruction_elementary(name, new RCond(c));
	//std::cout << "[INSTR]Cream un RCond | " << name << std::endl;
}

void create_instruction_LCond(std::string text) {
	char c = find_char(text);
	std::string name = "L(";
	name += c;
	name += ")";
	create_instruction_elementary(name, new LCond(c));
	//std::cout << "[INSTR]Cream un LCond | " << name << std::endl;
}

void create_instruction_RNonCond(std::string text) {
	char c = find_char(text);
	std::string name = "R(!";
	name += c;
	name += ")";
	create_instruction_elementary(name, new RNonCond(c));
	//std::cout << "[INSTR]Cream un RNonCond | " << name << std::endl;
}

void create_instruction_LNonCond(std::string text) {
	char c = find_char(text);
	std::string name = "L(!";
	name += c;
	name += ")";
	create_instruction_elementary(name, new LNonCond(c));
	//std::cout << "[INSTR]Cream un LNonCond | " << name << std::endl;
}

void create_instruction_label(std::string name) {
	//std::cout << "[INSTR]Cream un Label | " << name << std::endl;
	for (auto l : labels) {
		if (l->name == name) {
			current_state->set_next_state(l);
			return;
		}
	}
}

void create_instruction_machine(std::string name) {
	//std::cout << "[INSTR] Cream un Machine | " << name << std::endl;
	for (auto m : machines) {
		if (m.first == name) {
			State* s = new State("Instruction");
			s->set_name(name);

			add_new_state(s);
			return ;
		}
	}
}

void create_label(std::string name) {
	State* s = new State("Label");
	s->set_name(name);
	labels.push_back(s);

	add_new_state(s);
}

void create_decision() {
	State* s = new State("Decision");
	decisions.push(s);

	add_new_state(s);
	//std::cout << "[DECIZIE VAR] ";
	s->print_variables();
}

void end_decision() {
	if (! decisions.empty()) {
		decisions.pop();
		current_state = decisions.top();
	} 
}

void create_rule(char variable, std::list<char> &conditions) {
	State* s = new State("Rule");
	for (char c : conditions) {
		current_state->add_rule(c, s);
	}

	s->variables = current_state->variables;

	//std::cout << "[VARIABILA] Am primit variabila " << variable << std::endl;
	s->current_local_variable = variable;
	if (variable != '~') {
		s->variables.push_back({variable, '~'});
	}
	current_state = s;
}

void end_rule() {
	if (! decisions.empty()) {
		current_state = decisions.top();
	}
}

void start(std::string machine_name) {
	current_state = machines[machine_name];
	machines.erase(machine_name);
	while (1) {
		if (current_state == NULL) {
			if (! process_stack.empty()) {
				current_state = process_stack.top();
				process_stack.pop();
				continue ;
			} else {
				return ;
			}
		}
		//std::cout << "[INTOARCERE] S-a efectuat o intoarcere DIN " << current_state->type << std::endl;
		current_state->execute();
	}
}

















// Functii de print (pentru verificare)
void print_alphabet() {
	//std::cout << std::endl;
	//std::cout << std::endl;
	//std::cout << "Alfabetul este : [ ";

	for (auto c : alphabet) {
		//std::cout << c << " | ";
	}

	//std::cout << "]";
	//std::cout << std::endl;
}

void print_sets() {
	//std::cout << std::endl;
	//std::cout << "Seturile sunt  : " << std::endl;

	for (auto s : sets) {
		//std::cout << "	" << s.first << " [";
		for (auto c : *(s.second)) {
			//std::cout << c << " | ";
		}
		//std::cout << "]" << std::endl;
	}
	

	//std::cout << "]";
	//std::cout << std::endl;
}

void print_banda() {
	int i, j;
	for (i = banda.length()-1; i>=0; i--) {
		if (banda[i] != '#') {
			i++;
			break;
		}
	}
	for (j = 0; j < banda.length(); j++) {
		if (banda[j] != '#') {
			j--;
			break;
		}
	}
	std::cout << banda.substr(j, i-j+1) << std::endl;
//	out << banda.substr(j, i-j+1) << std::endl;
}
