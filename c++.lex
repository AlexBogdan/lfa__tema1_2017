    // An example of using the flex C++ scanner class.

%option c++

%top{

#include "cod.cpp"

std::list<char> conditions;
std::string temp_name; // Tine minte numele set-ului / masinii curent citite
char current_var_name = '~'; // Tinem numele ultimei variabile gasite; ~ = nimic

int call_source; // 0 - MACHINE | 1 - RULE

#define lastchar yytext[yyleng - 1]

}

%s ALPHABET NEW_LINE LINE_TYPE COMMENT SET MACHINE NEW_CALL RULE CONDITION NON_CONDITION
/* States:
   **(0) INITIAL: Starea initiala a masinii
   **(1) ALPHABET: Citeste fiecare caracter din alfabet
   **(1) NEW_LINE: Trecem pe o linie noua si hotaram daca este
   ****este un comentariu sau o masina
   **(1) LINE_TYPE: Stabileste tipul liniie ce urmeaza a fi parsata
   **(1) LINE_TYPE: Va crea un set cui caracterele citite
   **(3) COMMENT: Sare linia cu comentariu 
   **(4) MACHINE: Recunaoste o masina pe care o salveaza
   ****the file, or after a binary operator
   **(5) NEW_CALL: Verifica daca apelul este catre o eticheta, o masina
   ****elementara sau catre una creata anterior.
   **(6) RULE: Parsam fiecare regula una cate una, salvand in masina
   **** actuala
   **(7) CONDITION: Verificam pentru ce caractere vom merge pe respectiva
   **** tranzitie din conditie
   **(7) NON_CONDITION: Salvam caracterele pe care putem efectua tranzitia
 */

cap         [>]
diez        [#]
space       [ ]
tab         [\t]
new_line    [\n]
digit       [0-9]
alpha       [a-zA-Z]

alphanumeric {alpha}|{digit}
sep     ","
special "$"|"%"|"&"|"!"|"#"|"-"|"."|"/"|":"|"<"|">"|"="|"@"|"["|"]"|"^"|"`"|"~"|"+"|"*"|"_"
comment ";"
symbol {alpha}|{special}|{digit}
machine_name ("_"|{alphanumeric})*

banda_start {diez}{cap}{diez}
banda_elements ({alpha}|{diez})
banda_struct    {banda_start}{banda_elements}*


%%

{space}|{tab}

<INITIAL>{
    {symbol} {
        BEGIN(NEW_LINE);
    }
}
<NEW_LINE>{
    {comment} BEGIN(COMMENT);
    {machine_name}+ {
        temp_name = yytext; /* Extragem numele masinii/set-ului */
        BEGIN(LINE_TYPE);
    }
    {new_line} BEGIN(NEW_LINE);
}
<LINE_TYPE>{
    "::" {
        BEGIN(ALPHABET); /* Extragem alfabetul */
    }
    ("="|" :=") {
        create_set(temp_name);
        BEGIN(SET);
    }
    "::=" {
        /* Cream noua masina */
        create_machine(temp_name);

        /* Incepem constructia masinii */
        BEGIN(MACHINE);
    }
}
<ALPHABET>{
    {symbol} {
        add_to_alphabet(lastchar);
        BEGIN(ALPHABET);
    }
    {comment} BEGIN(NEW_LINE); /* Am terminat de citit alfabetul */
}
<SET>{
    {symbol} {
        add_to_set(lastchar);
        BEGIN(SET);
    }
    ("{"|","|"}") BEGIN(SET);
    {comment} BEGIN(NEW_LINE); /* Am terminat de setul */
}
<COMMENT>{
    ({space}|{sep}|{symbol})* BEGIN(COMMENT);
    {new_line} BEGIN(NEW_LINE);
}
<MACHINE>{
    {alphanumeric}+"@" { /* Am gasit o eticheta */
        /* Extragem eticheta */
        std::string name(yytext);
        name = name.substr(0, name.length()-1);
        /* Salvam eticheta in masina in care ne aflam */
        create_label(name);
    }
    "(" { /* Incepem sa parsam regulile */
        create_decision();
        BEGIN(RULE);
    }
    "[" { /* Am gasit un apel catre o masina */
        call_source = 0;
        BEGIN(NEW_CALL);
    }
    ";;" {
        labels.clear();
        BEGIN(NEW_LINE);
    }
    "\n" BEGIN(MACHINE);
}
<NEW_CALL>{ /* Verificam ce masina am gasit (in lista actuala de masini definite)*/
    "]" {
        if (call_source == 0) {
            BEGIN(MACHINE);
        }
        if (call_source == 1) {
            BEGIN(RULE);
        }
    }
    "R" {
        create_instruction_R();
        BEGIN(NEW_CALL);
    }
    "L" {
        create_instruction_L();
        BEGIN(NEW_CALL);
    }
    {symbol} {
        create_instruction_W(lastchar);
        BEGIN(NEW_CALL);
    }
    "<"{symbol}+">" {
        std::string set_name(yytext);
        set_name = set_name.substr(1, set_name.length() -2);
        for (auto s : sets) {
            if (s.first == set_name) {
                create_instruction_W(*(s.second->begin()));
                break;
            }
        }
        BEGIN(NEW_CALL);
    }
    "&"{symbol} {
        create_instruction_W(lastchar);
        BEGIN(NEW_CALL);
    }
    "R("{symbol}")" {
        std::string name = "";
        name += yytext[yyleng - 2];
        create_instruction_RCond(name);
        BEGIN(NEW_CALL);
    }
    "L("{symbol}")" {
        std::string name = "";
        name += yytext[yyleng - 2];
        create_instruction_LCond(name);
        BEGIN(NEW_CALL);
    }
    "R(!"{symbol}")" {
        std::string name = "";
        name += yytext[yyleng - 2];
        create_instruction_RNonCond(name);
        BEGIN(NEW_CALL);
    }
    "L(!"{symbol}")" {
        std::string name = "";
        name += yytext[yyleng - 2];
        create_instruction_LNonCond(name);
        BEGIN(NEW_CALL);
    }
    "R(<"{symbol}+">)" {
        std::string name(yytext);
        name = name.substr(3, name.length() -5);
        create_instruction_RCond(name);
        BEGIN(NEW_CALL);
    }
    "L(<"{symbol}+">)" {
        std::string name(yytext);
        name = name.substr(3, name.length() -5);
        create_instruction_LCond(name);
        BEGIN(NEW_CALL);
    }
    "R(!<"{symbol}+">)" {
        std::string name(yytext);
        name = name.substr(4, name.length() -6);
        create_instruction_RNonCond(name);
        BEGIN(NEW_CALL);
    }
    "L(!<"{symbol}+">)" {
        std::string name(yytext);
        name = name.substr(4, name.length() -6);
        create_instruction_LNonCond(name);
        BEGIN(NEW_CALL);
    }
    {machine_name}* {
        create_instruction_machine(yytext);
        BEGIN(NEW_CALL);
    }
}
<RULE>{
    {alphanumeric}"@<" { /* Am gasit o variabila pe o regula */
        conditions.clear();
        current_var_name = yytext[0];
        BEGIN(CONDITION);
    }
    {alphanumeric}"@!" { /* Am gasit o variabila pe o regula */
        conditions.clear();
        for (char c : alphabet) {
            conditions.push_back(c);
        }
        current_var_name = yytext[0];
        BEGIN(NON_CONDITION);
    }
    "{"|"<" {
        conditions.clear();
        BEGIN(CONDITION);
    }
    "!{" {
        conditions.clear();
        for (char c : alphabet) {
            conditions.push_back(c);
        }
        BEGIN(NON_CONDITION);
    }
    "->" BEGIN(RULE);
    "[" { /* Am gasit un apel catre o masina */
        call_source = 1;
        BEGIN(NEW_CALL);
    }
    {alphanumeric}+"@" { /* Am gasit o eticheta */
        /* Extragem eticheta */
        std::string name(yytext);
        name = name.substr(0, name.length()-1);
        /* Salvam eticheta in masina in care ne aflam */
        create_label(name);
    }
    "&"{alphanumeric}+ { /* Am gasit un goto */
        std::string name(yytext);
        name = name.substr(1, name.length()-1);
        create_instruction_label(name);
    }
    ";" {
        end_rule();
        BEGIN(RULE);
    }
    "(" { /* Incepem sa parsam regulile */
        create_decision();
        BEGIN(RULE);
    }
    ") ;" {
        end_decision();
        BEGIN(RULE);
    }
    ") ;;" {
        labels.clear();
        BEGIN(NEW_LINE);
    }
    {new_line} BEGIN(RULE);
}
<CONDITION>{
    {symbol} {
        conditions.push_back(lastchar);
        BEGIN(CONDITION);
    }
    "<"{symbol}+">" {
        std::string set_name = yytext;
        set_name = set_name.substr(1, set_name.length() -2);
        for (auto s : sets) {
            if (s.first == set_name) {
                for (char c : *(s.second)) {
                    conditions.push_back(c);
                }
            }
        }
    }
    {symbol}+">" {
        std::string set_name = yytext;
        set_name = set_name.substr(0, set_name.length() -1);
        for (auto s : sets) {
            if (s.first == set_name) {
                for (char c : *(s.second)) {
                    conditions.push_back(c);
                }
            }
        }
        create_rule(current_var_name, conditions);
        current_var_name = '~';
        BEGIN(RULE);
    }
    {sep} {
        BEGIN(CONDITION);
    }
    "}" {
        create_rule(current_var_name, conditions);
        current_var_name = '~';
        BEGIN(RULE);
    }
}
<NON_CONDITION>{
    {symbol} {
        conditions.remove(lastchar);
    }
    "<"{symbol}+">" {
        std::string set_name = yytext;
        set_name = set_name.substr(1, set_name.length() -2);
        for (auto s : sets) {
            if (s.first == set_name) {
                for (char c : *(s.second)) {
                    conditions.remove(c);
                }
            }
        }
        BEGIN(NON_CONDITION);
    }
    "{<"{symbol}+">" {
        std::string set_name = yytext;
        set_name = set_name.substr(2, set_name.length() -3);
        for (auto s : sets) {
            if (s.first == set_name) {
                for (char c : *(s.second)) {
                    conditions.remove(c);
                }
            }
        }
        BEGIN(NON_CONDITION);
    }
    "}" {
        create_rule(current_var_name, conditions);
        current_var_name = '~';
        BEGIN(RULE);
    }
}
    
%%

extern "C" {
  int yywrap();
}

int yyFlexLexer::yywrap() {
  return ::yywrap();
}

int main( int  argc , char**  argv  ) {


    std::ifstream in(argv[1]);
    current_state = new State("Start");

    FlexLexer* lexer = new yyFlexLexer(&in);
    while(lexer->yylex() != 0);

    banda = argv[3];
    std::string extend(banda.length()*6, '#');
    banda += extend;

    extend += banda;
    banda = extend;

    for (cap_banda = banda.begin(); *cap_banda != '>'; cap_banda++);
    cap_banda ++;

    

    start(argv[2]);


    print_alphabet();
    print_sets();
    print_banda();

    return 0;
}