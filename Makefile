CC=g++
LEX=flex
LDFLAGS=-lfl -std=c++11
CFLAGS=-c -g -Wall
OFLAGS=-o $@
EXEC = mtx
SOURCES = $(wildcard *.c)
FLEXES = $(wildcard *.l)
OBJECTS = $(SOURCES:.c=.o) $(FLEXES:.l=.o)

all: build

build: $(EXEC)

run: build
	./$(EXEC) '$(value arg1)' '$(value arg2)' '$(value arg3)'

$(EXEC): $(OBJECTS)
	$(LEX)  c++.lex && $(CC) $(OFLAGS) "lex.yy.cc" $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) $(OFLAGS) $<

%.c: %.l
	$(LEX) -o $@ $<

.PHONY: clean

clean:
	$(RM) *~ $(OBJECTS) $(EXEC) &> /dev/null

pack: clean
	zip archive_`date +'%s'`.zip README Makefile *.c *.h *.cpp *.hpp *.cxx *.hxx *.cc *.hh *.c++ *.h++ *.C *.H *.java *.l *.lex *.flex
